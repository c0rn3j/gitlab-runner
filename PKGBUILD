# Maintainer: Sven-Hendrik Haase <svenstaro@archlinux.org>
# Maintainer: Caleb Maclennan <caleb@alerque.com>
# Contributor: Lubomir 'Kuci' Kucera <kuci24-at-gmail-dot-com>

pkgname=gitlab-runner
pkgver=16.9.1
pkgrel=1
pkgdesc="The official GitLab CI runner written in Go"
arch=('x86_64')
url='https://gitlab.com/gitlab-org/gitlab-runner'
license=('GPL3')
depends=('ca-certificates' 'curl' 'git' 'glibc' 'tar')
optdepends=('inetutils: hostname command')
makedepends=('git' 'go' 'git' 'mercurial' 'gox')
install=gitlab-runner.install
replaces=('gitlab-ci-multi-runner')
backup=('etc/gitlab-runner/config.toml')
noextract=("prebuilt-alpine-arm-${pkgver}.tar.xz"
           "prebuilt-alpine-arm64-${pkgver}.tar.xz"
           "prebuilt-alpine-s390x-${pkgver}.tar.xz"
           "prebuilt-alpine-x86_64-pwsh-${pkgver}.tar.xz"
           "prebuilt-alpine-x86_64-${pkgver}.tar.xz"
           "prebuilt-ubuntu-arm-${pkgver}.tar.xz"
           "prebuilt-ubuntu-arm64-${pkgver}.tar.xz"
           "prebuilt-ubuntu-s390x-${pkgver}.tar.xz"
           "prebuilt-ubuntu-x86_64-pwsh-${pkgver}.tar.xz"
           "prebuilt-ubuntu-x86_64-${pkgver}.tar.xz")
source=("git+https://gitlab.com/gitlab-org/gitlab-runner.git#tag=v${pkgver}"
        "prebuilt-alpine-arm-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-arm.tar.xz"
        "prebuilt-alpine-arm64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-arm64.tar.xz"
        "prebuilt-alpine-s390x-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-s390x.tar.xz"
        "prebuilt-alpine-x86_64-pwsh-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-x86_64-pwsh.tar.xz"
        "prebuilt-alpine-x86_64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-x86_64.tar.xz"
        "prebuilt-ubuntu-arm-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-arm.tar.xz"
        "prebuilt-ubuntu-arm64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-arm64.tar.xz"
        "prebuilt-ubuntu-s390x-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-s390x.tar.xz"
        "prebuilt-ubuntu-x86_64-pwsh-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-x86_64-pwsh.tar.xz"
        "prebuilt-ubuntu-x86_64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-x86_64.tar.xz"
        "gitlab-runner.service"
        "gitlab-runner.sysusers"
        "gitlab-runner.tmpfiles"
        "config.toml")
sha512sums=('SKIP'
            '0c2fa663c3b3746a47ea7f54fd8f3f3ea0a21a0aa24b14a023b34db842a85c4eb24a3bf694d9a42d626928e081e48d38752c96f2bd19f1b90138cb9ffaf2019d'
            '309bfa7c151139d0bf4d9e1aaa1b076e3dacad8e03b4ab59353d95a3c8df1d8c67484beac6c0506c17cfd3ec41e8c34d803e65d6bda5ec182781b45a83e13926'
            'f762693783f9eb92012b4f3ac4926a5fb9b779f61398c0aefb5e34047d53ff7653dbeae67f556068fdffc548ea7bbb516f26b83ad787aa2d455966fb68e9b2cc'
            'fc22a416d5f0b4994607cc2d5e58e047f007df522b091589cfe8a687edb67822ca4c9a50a566814ff91e4619f0ff84dceec640691b0818ea12ab672e15b7c14b'
            'bc8e41a3f9bd92db8ab9028cc72f89f48bd84e6dd263b3d578295b5ff11422693465bfff4667a0c8470aca8ce0067503d7080410282fb9081504bc9a8e60575c'
            '995d7c79774dd74291cdfdc6d119012979eadad3a4cf8abc6424179375522ccbe6020e81dd3b5a85f79e615bdc3e7c8de4fe923a9acc4e3ed825a6a0737c45f4'
            'b3a55ea3cef199e64136766e3b5cea69b4137309febe1c0456e1b3b8310bb55acc04fd573c7505b62e73389f50dc2168463f0b7f80764931f745d085bacb861b'
            '427be7eefa236cedc68203f6c08acd330900eeacf85de4a727ee9c56b0cc95d9bf19edfe9fdf9d50e90cbb07b3416fe41740ac3180ba47f88b49f72680ce6c06'
            '15f63af519939562f10c49b33d6c0101bf06f99cdb4e0f6a029461fe34c469793e6a336d9d5fc13d687aaa67b4e43f11d6faf57cb6ec509889f7278264205216'
            '6cc28271f4acf4eed0b466b6f97a1db2ef55b88642f606f784ce698a5bb248388108ecf8e38095af3c59962e3c60f13b354c034b32749a042fd64ba49c5eae44'
            'c0af374b9986895aedcfaee6c67cfad68f0f7289f87e4611358adaff59a2f349f55764fe28b2b1f61f8bfeb61126d4f90d433c626fdf9b826a2de6217f86574f'
            '8aa7f08702e99053c696fcc2aaba83beb9e9cd6f31973d82862db9350ac46df3a095377625d31fe909677525290d2de922d7a97930ed235774cb8f0da8944d40'
            '6751d9fa0b27172d1b419c4138f5ac15cbc7c9147653a7258cf1470216142c637210bb60608c7ed0974e0e4057e5ddeae32225df1bb36e7dd1f20fec71e33cc3'
            '9718b94bd0ddb09095ffb8c1e60ca1e9649dabb1747e7fc95e58e404b2f9effdeb4cfd759f5b904443dc53a4e18c02003c38f85584713deb49f6a6d1007503de')

prepare() {
  cd gitlab-runner

  local version=$(make version | grep "Current version:" | sed -n "s/.*: \(.*\)/\1/p")
  local revision=$(make version | grep "Current revision:" | sed -n "s/.*: \(.*\)/\1/p")
  local branch=$(make version | grep "Current branch:" | sed -n "s/.*: \(.*\)/\1/p")

  sed -i "s/var VERSION.*/var VERSION = \"$version\"/" common/version.go
  sed -i "s/var REVISION.*/var REVISION = \"$revision\"/" common/version.go
  sed -i "s/var BRANCH.*/var BRANCH = \"$branch\"/" common/version.go
}

build() {
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -ldflags=-linkmode=external -mod=readonly -modcacherw"

  cd gitlab-runner
  go build -o gitlab-runner .
}

package() {
  cd gitlab-runner

  install -Dm644 "${srcdir}/config.toml" "${pkgdir}/etc/gitlab-runner/config.toml"
  install -Dm644 "${srcdir}/gitlab-runner.service" "${pkgdir}/usr/lib/systemd/system/gitlab-runner.service"
  install -Dm644 "${srcdir}/gitlab-runner.sysusers" "${pkgdir}/usr/lib/sysusers.d/gitlab-runner.conf"
  install -Dm644 "${srcdir}/gitlab-runner.tmpfiles" "${pkgdir}/usr/lib/tmpfiles.d/gitlab-runner.conf"
  install -Dm755 gitlab-runner "${pkgdir}/usr/bin/gitlab-runner"
  install -Dm755 packaging/root/usr/share/gitlab-runner/clear-docker-cache "${pkgdir}/usr/share/gitlab-runner/clear-docker-cache"

  # Move prebuilt Docker images to hard-coded canonical location
  for image in prebuilt-{alpine,ubuntu}-{arm,arm64,s390x,x86_64-pwsh,x86_64}-${pkgver}.tar.xz; do
    install -Dm644 "${srcdir}/${image}" "${pkgdir}/usr/lib/gitlab-runner/helper-images/${image}"
  done
}
